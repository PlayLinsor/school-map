/*
*		Основной движок
*/

var mymap = L.map('mapid').setView([45.59867, 34.4751], 10);
var ge = "";
// Иконки
var IconBlue = L.icon({
	
	iconUrl: 'images/icon-blue.png',
	iconSize: [25, 41],
	iconAnchor: [13, 41],
	popupAnchor: [0, -44],
});
		
var IconGreen = L.icon({
	
	iconUrl: 'images/icon-green.png',
	iconSize: [25, 41],
	iconAnchor: [13, 41],
	popupAnchor: [0, -44],
});

// Загрузка карты
L.tileLayer('yMaps/z{z}-x{x}-y{y}.png', {
			maxZoom: 14,
			minZoom: 10,
			attribution: 'Разработка: Наумко Дмитрий | Редакция 18.07.2016',
			id: 'mapbox.streets'
		}).addTo(mymap);

// Прогрузка маркеров		
for (var key in mainDB){
	var info = mainDB[key];
	var IconLocation = IconBlue;
	if (info[0]==2) IconLocation = IconGreen;
	
	L.marker([info[1], info[2]], {icon: IconLocation}).addTo(mymap).bindPopup("<b>"+info[3]+"</b><br />"+info[4]+"<br/><br /><a href='detail/"+info[5]+"'>Подробнее</a>");
}

// Хз что это
var popup = L.popup();


		function onMapClick(e) {
			ge = e;
			popup
				.setLatLng(e.latlng)
				.setContent("Координаты: " + e.latlng.toString())
				//.setContent("Координаты: [" + e.latlng.lng.toFixed(5) + " , " +e.latlng.lat.toFixed(5)+"],")
				.openOn(mymap);
		}
		
//mymap.on('click', onMapClick);
		



/*
	Дополнения
*/

var c = document.getElementsByClassName('leaflet-control-zoom');
var alink = document.createElement('a');
alink.className = 'leaflet-control-zoom-in';
alink.innerHTML = 'C';
alink.setAttribute('href','#');
alink.setAttribute('title','По центру');
alink.setAttribute('onClick','OnCenter()');

c[0].appendChild(alink);

function OnCenter(){
	mymap.setView([45.59867, 34.4751], 10);
}


